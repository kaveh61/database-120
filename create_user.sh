#!/bin/bash

NEW_USER=$1
NEW_PASSWORD=$2

echo "Creating the user..."
mysql -u root <<EOF
CREATE USER '$NEW_USER'@'localhost' IDENTIFIED BY '$NEW_PASSWORD';
GRANT ALL PRIVILEGES ON *.* TO '$NEW_USER'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;
EOF
echo "User created."
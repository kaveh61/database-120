# README.md

## Project Overview

This project involves setting up two Ubuntu 22.04 LTS servers available as AWS EC2 instances and installing Percona MySQL on them. Post-installation, a MySQL user named 'abo' is created.

Below additional features will be added thereafter:
- Logging
- Instance-wide replication setup as a fail-safe 


## Server Preparation

An Ansible playbook `prepare_server.yaml` is used for the preparation of the servers, which involves several steps:

1. Creating a group and a user named 'ansible'.
2. Setting up passwordless sudo for the 'ansible' user.
3. Adding an SSH key to the 'ansible' user.
4. Adding the IP of the secondary server to the /etc/hosts file on the primary server and vice versa for seamless communication using their hostnames.

## Percona MySQL Installation

After the servers are prepared, Percona MySQL 5.7 is installed on both servers following the official Percona documentation: https://docs.percona.com/percona-server/5.7/installation/apt_repo.html#whats-in-each-deb-package.


## User Creation

A shell script is used for this purpose that accepts two parameters: username and password. This script creates the 'abo' user in MySQL and grants all privileges on all databases to this user.

The script is invoked by an Ansible playbook `deploy_script.yaml` which fetches the username and password from an encrypted Ansible vault file, keeping the credentials secure.

## Verification

Finally, another Ansible playbook `verofy_percona.yaml` is run to verify the success of the performed tasks. This playbook checks:

1. If the MySQL service is running.
2. Whether the 'abo' user has been created successfully.
3. The privileges granted to the 'abo' user.

The output from each of these checks is displayed for verification.

## MySQL Replication

MySQL instance-wide Master-Slave replication has been implemented to add a layer of redundancy and failover capability. This is achieved using Ansible tasks and built-in MySQL replication capabilities in Ansible playbook `setup_replication.yaml`

The steps involved in this process include:

1. Installing the necessary system packages and Python modules.
2. Setting unique server IDs for each server and enabling binary logging on the primary.
3. Utilizing `abo` as the replication user on the primary server.
4. Getting the primary server's binlog file name and binlog position.
5. Configuring the replica server using the primary server's details and starting the replica.

## Logging

For monitoring purposes, General query logs, Audit logs, and Error logs have been enabled on both servers using Ansible playbook `enable_logging.yaml`

A Jinja2 template `templates\my.cnf.j2` of the MySQL configuration file is used to customize the setup. Ansible tasks are used to:

1. Install the audit plugin for MySQL.
2. Deploy the MySQL configuration file from the Jinja2 template.
3. Restart the MySQL service to apply the changes.

